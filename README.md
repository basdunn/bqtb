# bqtb - Bigquery Toolbox


## How to develop

- Use VS Code devcontainers

or 

- Run inside a docker container: `docker build . --target devcontainer -t bqtb` and `docker run -it bqtb bash` and `poetry install` (inside the container).