FROM python:3.10 AS base

RUN pip3 install poetry==1.5.1
RUN poetry config virtualenvs.create false


FROM base AS prod 

WORKDIR /bqtb_workdir
COPY poetry.lock pyproject.toml README.md ./
COPY bqtb ./bqtb
RUN poetry install --only main


FROM base AS devcontainer

RUN curl -sSL https://sdk.cloud.google.com | bash
ENV PATH $PATH:/root/google-cloud-sdk/bin