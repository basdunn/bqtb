from google.cloud import bigquery

class BigqueryClient():
    def __init__(self):
        self.bqclient = bigquery.Client()

    def projects(self):
        return list(self.bqclient.list_projects(max_results=10))